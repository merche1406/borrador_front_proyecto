import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
/**
 * @customElement
 * @polymer
 */
class VisorMov extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          }

   }
  /*        all:initial; */

      </style>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
       <h1>Sus movimientos</h1>

-->
  <dom-repeat items="{{movimientos}}">
  <template>
   <h3>IBAN - {{item.IBAN}}</h3>
   <h3>Fecha - {{item.Fecha}}</h3>
   <h3>Concepto - {{item.Concepto}}</h3>
   <h3>Importe - {{item.Importe}}</h3>
   <h3>Saldo - {{item.Saldo}}</h3>
 </template>
 </dom-repeat>

  
 <iron-ajax
 auto
 id="getmovement"
 url="http://localhost:3000/apitechu/v2/movement/{{id}}"
 handle-as="json"
 on-response="showData"
 ></iron-ajax>
    `;
  }
  static get properties() {
    return {
      id : {
        type: String
        // ,
        // observer: "_useribanChanged"
      }, movimientos: {
        type: Array
      }
    };
  }

  showData(data) {
    console.log(data.detail.response);
    this.movimientos = data.detail.response;
  }

  // _useribanChanged() {
  //   console.log("estoy pasando por colsultar movimmiento");
  //   this.$.getmovement.generateRequest();
  // }
  }

window.customElements.define('visor-mov', VisorMov);
