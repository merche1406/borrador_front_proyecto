import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
/**
 * @customElement
 * @polymer
 */
class LoginUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          }
    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <h2>Login Usuario</h2>
    <input type="email" placeholder="email" value="{{email::input}}"></imput>
    <input type="password" placeholder="password"value="{{password::input}}"></imput>

     <button on-click="login">Login </button>


    <span hidden$="{{!isLogged}}">Bienvenido/a de nuevo</span>

   <iron-ajax
      id="doLogin"
      url="http://localhost:3000/apitechu/v2/login"
      handle-as="json"
      method ="POST"
      content-type= "application/json"
      on-response="manageAJAXResponse"
      on-error="showError"
   >
  

    `;
  }
  static get properties() {
    return {
      email: {
        type: String
      },password:{
        type:String
      },isLogged:{
        type:Boolean,
        value:false
      }
    };
  } // Fin de prpperties

 login(){
  console.log("El Usuario ha pulsado el boton");
    console.log("voy a enviar la peticion");

    var loginData ={
      "email" : this.email,
      "password" : this.password
    }
    console.log(loginData);

    this.$.doLogin.body = JSON.stringify(loginData);  //lo trasnformo en json los datos
    this.$.doLogin.generateRequest();
    console.log ("Peticion enviada");
 }

 showError(error){
  console.log("Hubo un error");
  console.log(error);
 }
  manageAJAXResponse(data){
    console.log("manageAJAXResponse");
    console.log(data.detail.response);
    this.isLogged =true;
    this.dispatchEvent(
      new CustomEvent(
        "loginsucess",
        {
          "detail" : {
            "userid" : data.detail.response.idUsuario
          }
        }
      )
    )

  }
}  //End de la clase


window.customElements.define('login-usuario', LoginUsuario);
