import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
/**
 * @customElement
 * @polymer
 */
class VisorUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>
      <h2>Soy [[first_name]] [[last_name]]</h2>
        <h3>y mi email es [[email]]</h3>

<iron-ajax
auto
id="getUser"
url="http://localhost:3000/apitechu/v2/users/{{id}}"
handle-as="json"
on-response="showData"
></iron-ajax>

    `;
  }
  static get properties() {
    return {
      first_name: {
        type: String
      },last_name: {
        type: String
      },email: {
        type: String
      },id:{
        type:Number
      }
    };
  } // Fin de prpperties

  showData(data){
    console.log("showData");
    console.log(data.detail.response);
    this.first_name =data.detail.response.first_name;
    this.last_name = data.detail.response.last_name;
    this.email = data.detail.response.email;
  }
}  //End de la clase

window.customElements.define('visor-usuario', VisorUsuario);
