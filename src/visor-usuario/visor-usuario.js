import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
import '../visor-cuentas/visor-cuentas.js';
/**
 * @customElement
 * @polymer
 */
class VisorUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
        }
      </style>

      <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
      <h2>Soy [[first_name]] [[last_name]]</h2>
      <h3>y mi email es [[email]]</h3>
      <visor-cuentas id="visorCuenta"></visor-cuentas>
<!--       <crear-cuentas id="crearCuenta"></crear-cuentas> -->
      <button on-click="showAccounts">Ver mis cuentas</button>
      <button on-click="newAccounts">Crear una cuenta</button>
      <button on-click>Hacer tranferencia</button>
      <button on-click>Consultar movimientos</button>
      <iron-ajax
      id="getUser"
      url="http://localhost:3000/apitechu/v2/users/{{id}}"
      handle-as="json"
      on-response="showData"
      ></iron-ajax>

    `;
  }
  static get properties() {
    return {
      first_name: {
        type: String
      }, last_name: {
        type: String
      }, email: {
        type: String
      }, id: {
        type: Number,
        observer: '_useridChanged'
      }
    };
  }

  showData(data){
    console.log("showData");
    console.log(data.detail.response);
    this.first_name =data.detail.response.first_name;
    this.last_name = data.detail.response.last_name;
    this.email = data.detail.response.email;
  }

_useridChanged(newValue, oldValue) {
  this.$.getUser.generateRequest();
}

newAccounts(e){
    console.log("crear nueva cuenta: ")
    console.log(this.id)
    console.log("que le paso a crear  cuenta");
    console.log(e.detail.id);
    this.$.crearCuentas.id = e.detail.id;
    }
showAccounts(){
  console.log("valor que falla: ")
  console.log(this.id)

  console.log("que le paso a visor de  cuenta");
  console.log(this.$.visorCuenta.id);
  this.$.visorCuenta.id = this.id;
  }


}
window.customElements.define('visor-usuario', VisorUsuario);
