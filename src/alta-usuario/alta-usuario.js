import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
/**
 * @customElement
 * @polymer
 */
class AltaUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          }
    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <h2>Alta Usuario</h2>
    <input type="first_name" placeholder="first_name" value="{{first_name::input}}"></imput>
    <input type="last_name" placeholder="last_name" value="{{last_name::input}}"></imput>
    <input type="email" placeholder="email" value="{{email::input}}"></imput>
    <input type="password" placeholder="password"value="{{password::input}}"></imput>

     <button on-click="registro">Registro de un usuario nuevo </button>
<!--     <button on-click="creacuenta">Crear cuenta nueva </button>  -->

     <span hidden$="{{!existe}}"> NO se ha podido dar de alta el correo indicado</span>

   <iron-ajax
      id="doRegistro"
      url="http://localhost:3000/apitechu/v2/users"
      handle-as="json"
      method ="POST"
      content-type= "application/json"
      on-response="manageAJAXResponse"
      on-error="showError"
   >


    `;
  }
  static get properties() {
    return {
      first_name: {
        type: String
      },last_name:{
        type:String
      },email: {
        type: String
      },password:{
        type:String
      },isLogged:{
        type:Boolean,
        value:false
      },existe:{
        type:Boolean,
        value:false
      }
    };
  } // Fin de prpperties

 registro(){
  console.log("El Usuario ha pulsado el boton");
  console.log("voy a enviar la peticion");

    var loginData ={
      "first_name": this.first_name,
      "last_name": this.last_name,
      "email" : this.email,
      "password" : this.password,
      "id": 50
    }
    console.log(loginData);

    this.$.doRegistro.body = JSON.stringify(loginData);  //lo trasnformo en json los datos
    this.$.doRegistro.generateRequest();
    console.log ("Peticion de registro de nuevo usuario enviada");
 }

 showError(error){
  console.log("Hubo un error en el resgistro del usuario");
  console.log(error);
 }
  manageAJAXResponse(data){
     console.log("manageAJAXResponse");
     console.log(data.detail.response);
     console.log()
     if (data.detail.response.idUsuario==0){
       this.existe=true;
       console.log(this.existe);
     }

  }
}  //End de la clase


window.customElements.define('alta-usuario', AltaUsuario);
