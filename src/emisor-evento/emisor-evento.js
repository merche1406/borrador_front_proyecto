import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
/**
 * @customElement
 * @polymer
 */
class EmisorEvento extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          }
    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <h2>Soy el emisor</h2>

    <button on-click="sendEvent">No pulsar </button>

    `;
  }
  static get properties() {
    return {

    };
  } // Fin de prpperties

 sendEvent(e){
   console.log(" El usuario ha pulsado el boton");
   console.log(e);

   this.dispatchEvent(
      new CustomEvent(
        "myevent",   //primer parámetro nombre del evento
        {
          "detail":{    //dentro de este objet los que queramos transmitir al exterior
            "course": "TechU",
            "year": 2019
          }
        }
      )
   )
 }


}  //End de la clase

window.customElements.define('emisor-evento', EmisorEvento);
