import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
/**
 * @customElement
 * @polymer
 */
class VisorCuentas extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          }

   }
  /*        all:initial; */

      </style>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
       <h1>Sus cuentas</h1>
<!--       <h1>Sus cuentas</h1>
    <h3>IBAN - {{iban}}</h3>
    <h3>Saldo - {{balance}}</h3>
-->
  <dom-repeat items="{{accounts}}">
  <template>
   <h3>IBAN - {{item.IBAN}}</h3>
   <h3>Saldo - {{item.balance}}</h3>
 </template>
 </dom-repeat>

 <iron-ajax
 id="getaccount"
 url="http://localhost:3000/apitechu/v2/account/{{id}}"
 handle-as="json"
 on-response="showData"
 ></iron-ajax>
    `;
  }
  static get properties() {
    return {
      id : {
        type: Number,
        observer: "_useridChanged"
      }, accounts: {
        type: Array
      }
    };
  }

  showData(data) {
    console.log(data.detail.response);
    this.accounts = data.detail.response;
  }

  _useridChanged() {
    console.log("estoy pasando por useridChanged");
    this.$.getaccount.generateRequest();
  }
  }

window.customElements.define('visor-cuentas', VisorCuentas);
