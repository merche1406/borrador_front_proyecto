import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
/**
 * @customElement
 * @polymer
 */
class CrearCuentas extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          }

   }
  /*        all:initial; */

      </style>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
       <h1>Crear una nueva cuenta</h1>


 <!-- auto  -->
 <iron-ajax
 id="createaccount"
 url="http://localhost:3000/apitechu/v2/account/{{id}}"
 handle-as="json"
 method ="POST"
 on-response="showData"
 >

    `;
  }

  static get properties() {
    return {
      id : {
        type: Number,
        observer: "_useridChanged"
      }
    };
  } // Fin de prpperties

  // createaccount(){
  //  console.log("voy a enviar la peticion de creación de cuenta");
  //  console.log ("Peticion enviada");
  // }
  _useridChanged() {
    console.log("estoy pasando por useridChanged");
    this.$.createaccount.generateRequest();
  }
  showData(data) {
    console.log(data.detail.response);

  }

  }

window.customElements.define('crear-cuentas', CrearCuentas);
