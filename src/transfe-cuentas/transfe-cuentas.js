import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
/**
 * @customElement
 * @polymer
 */
class TransfAccount extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          }
    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <h2>Tranferencia de cuentas</h2>
    <input type="origen" placeholder="Origen" value="{{Origen::input}}"></imput>
    <input type="destino" placeholder="Destino"value="{{Destino::input}}"></imput>
    <input type="concepto" placeholder="Concepto" value="{{Concepto::input}}"></imput>
    <input type="importe" placeholder="Importe"value="{{Importe::input}}"></imput>

   <button on-click="transfe">Tranferencia </button>

   <iron-ajax
      id="doTransfe"
      url="http://localhost:3000/apitechu/v2/transfer"
      handle-as="json"
      method ="POST"
      content-type= "application/json"
      on-response="manageAJAXResponse"
      on-error="showError"
   >

    `;
  }
  static get properties() {
    return {
    origen: {
        type: String
    },destino:{
        type:String
      },concepto: {
          type: String
        },importe:{
          type:String
          }
    };
  } // Fin de prpperties

 transfe(){
  console.log("El Usuario ha pulsado el boton");
  console.log("voy a enviar la peticion");

    var transferData ={
      "Origen" : this.Origen,
      "Destino" : this.Destino,
      "Concepto" : this.Concepto,
      "Importe" : this.Importe
    }
    console.log(transferData);

    this.$.doTransfe.body = JSON.stringify(transferData);  //lo trasnformo en json los datos
    this.$.doTransfe.generateRequest();
    console.log ("Peticion enviada");
 }

 showError(error){
  console.log("Hubo un error");
  console.log(error);
 }
  manageAJAXResponse(data){
    console.log("manageAJAXResponse");
    console.log(data.detail.response);
    }
}  //End de la clase


window.customElements.define('transfe-cuentas', TransfAccount);
