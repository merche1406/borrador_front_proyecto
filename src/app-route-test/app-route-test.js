import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-pages/iron-pages.js';
import '../visor-usuario/visor-usuario.js';
import '../login-usuario/login-usuario.js';
import '@polymer/app-route/app-route.js';
import '@polymer/app-route/app-location.js';

/**
 * @customElement
 * @polymer
 */
class AppRouteTest extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          }
    </style>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <h1 class="jumbotron"> App Route Test </h1>
<!--     <select value="{{componentName::change}}" placeholder="Seleccionar componente">
       <option value=""> Seleccionar componente</option>
       <option value="visor-usuario"> Visor usuario</option>
       <option value="login-usuario"> Login usuario</option>
    </select> -->
    <app-location route="{{route}}"> </app-location>
    <app-route
       route="{{route}}"
       pattern ="/:resource"
       data="{{routeData}}"
    >
    </app-route>
    <app-route
       route="{{route}}"
       pattern ="/:resource/:id"
       data="{{routeData}}"
    >
    </app-route>

    <iron-pages selected="[[routeData.resource]]" attr-for-selected="component-name">
      <div component-name="user"> <visor-usuario id="[[routeData.id]]"> </visor-usuario></div>
      <div component-name="login"> <login-usuario> </login-usuario></div>
    </iron-pages>

    `;
  }

  static get properties() {
    return {
      route:{
        type : Object
      }, routeData :{
        type: Object
      }
    };
  } // Fin de prpperties


}  //End de la clase

window.customElements.define('app-route-test', AppRouteTest);
