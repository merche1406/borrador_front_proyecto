import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '../login-usuario/login-usuario.js';
import '../visor-usuario/visor-usuario.js';

/**
 * @customElement
 * @polymer
 */
class PanelUsuario extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          }

  /*        all:initial; */

      </style>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <h1>Yo soy tu padre</h1>
        <login-usuario on-loginsucess="processLoginSuccess"></login-usuario>
        <visor-usuario id="visorUsuario"></visor-usuario>

    `;
  }
  static get properties() {
    return {
    };
  } // Fin de prpperties

  processLoginSuccess(e) {
    console.log("Proceso de Login correcto para usuario :");
    console.log(e.detail.userid);
    this.$.visorUsuario.id = e.detail.userid;
  }
}  //End de la clase

window.customElements.define('panel-usuario', PanelUsuario);
