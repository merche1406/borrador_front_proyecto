import {html, PolymerElement} from '@polymer/polymer/polymer-element.js';
import '@polymer/iron-ajax/iron-ajax.js';
/**
 * @customElement
 * @polymer
 */
class BorrarCuentas extends PolymerElement {
  static get template() {
    return html`
      <style>
        :host {
          display: block;
          }

   }
  /*        all:initial; */

      </style>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
       <h1>Borrar una cuenta</h1>


 <!-- auto  -->
 <iron-ajax
 auto
 id="deleteaccount"
 url="http://localhost:3000/apitechu/v2/account/{{id}}"
 handle-as="json"
 method ="DELETE"
 on-response="showData"
 >

    `;
  }

  static get properties() {
    return {
      id : {
        type: String
      }
    };
  } // Fin de prpperties


  showData(data) {
    console.log(data.detail.response);

  }

  }

window.customElements.define('borrar-cuentas', BorrarCuentas);
